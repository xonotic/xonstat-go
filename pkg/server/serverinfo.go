package server

import (
	"html/template"

	"github.com/antzucaro/qstr"
	"gitlab.com/xonotic/xonstat/pkg/leaderboard"
	"gitlab.com/xonotic/xonstat/pkg/models"
)

// TopScorerBase is the base type for players on a server and their
// accumulated score
type TopScorerBase struct {
	SortOrder    int
	PlayerID     int
	Nick         string
	NickStripped string
	NickHTML     template.HTML
	Score        int
}

// TopScorerData returns view-agnostic data for the top scorers on a given server.
func TopScorerData(db models.Datastore, serverID int) ([]*TopScorerBase, error) {
	rawActivePlayerScores, err := db.RServerActivePlayerScores(serverID, 10)
	if err != nil {
		return nil, err
	}

	var topScorers []*TopScorerBase
	for _, v := range rawActivePlayerScores {
		nick := models.NewMultiNick(v.Nick)
		ts := TopScorerBase{
			SortOrder:    v.SortOrder,
			PlayerID:     v.PlayerID,
			Nick:         v.Nick,
			NickStripped: nick.NickStripped,
			NickHTML:     nick.NickHTML,
			Score:        v.Score,
		}

		topScorers = append(topScorers, &ts)
	}
	return topScorers, nil
}

// TopActivePlayersData returns view-agnostic data for the most active players on a given server.
// NOTE: the base type returned here is shared with the leaderboard package.
func TopActivePlayersData(db models.Datastore, serverID int) ([]*leaderboard.ActivePlayerBase, error) {
	// Top players by alive time over the time period.
	rawActivePlayers, err := db.RActivePlayersByServer(serverID, 10)
	if err != nil {
		return nil, err
	}

	activePlayers := leaderboard.ActivePlayerToActivePlayerBase(rawActivePlayers)
	return activePlayers, nil
}

// TopMapsData returns view-agnostic data for the most active maps on a given server by times played.
// NOTE: there is no base type here (it is straight from model/query result) because no manipulation
// or hiding is required.
func TopMapsData(db models.Datastore, serverID int) ([]*models.ActiveMap, error) {
	// Top maps by times played over the time period.
	activeMaps, err := db.RActiveMapsByServer(serverID, 10)
	if err != nil {
		return nil, err
	}

	return activeMaps, nil
}

// InfoBase is the base type used to represent servers for all
// marshalled types (HTML/JSON/etc).
type InfoBase struct {
	ServerID  int
	Name      string
	NameHTML  template.HTML
	IPAddr    string
	Port      int
	Revision  string
	ActiveInd bool
	CreateDt  *models.MultiDt
}

// InfoData retrieves information about a given server.
func InfoData(db models.Datastore, serverID int) (*InfoBase, error) {
	rawServer, err := db.RServerByID(serverID)
	if err != nil {
		return nil, err
	}

	// Conversions.
	name := qstr.QStr(rawServer.Name.String)
	dt, err := models.NewMultiDt(rawServer.CreateDt)
	if err != nil {
		return nil, err
	}

	return &InfoBase{
		ServerID:  rawServer.ServerID,
		Name:      rawServer.Name.String,
		NameHTML:  name.HTML(),
		IPAddr:    rawServer.IPAddr.String,
		Port:      int(rawServer.Port.Int64),
		Revision:  rawServer.Revision.String,
		ActiveInd: rawServer.ActiveInd,
		CreateDt:  dt,
	}, nil
}
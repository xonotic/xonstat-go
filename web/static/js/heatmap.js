var url = "/heatmap";
if (server_id != -1) {
    url = url + "?server_id=" + server_id;
}

d3.json(url, {
    headers: new Headers({
        "Accept": "application/json"
    }),
}).then((data) => {

    var gameCounts = data.map(d => d[2]);
    var maxGames = Math.max(...gameCounts);

    // Localize a UTC day-of-week and hour, leaving gameCount the same.
    function localize(dayUTC, hourUTC, gameCount) {
        // May 2023 is used because the days of its first week happen to match 
        // PostgreSQL's isodow() day index. Mon is 1, Tues is 2, and so on.
        d = new Date(Date.UTC(2023, 4, dayUTC, hourUTC));

        // PG's isodow() returns 1-7 for Mon-Sun. JS's getDay() returns 0-6 for Sun-Sat.
        // The difference is only Sun (0 -> 7).
        dayLocal = d.getDay();
        return [dayLocal == 0 ? 7 : dayLocal, d.getHours(), gameCount];
    }

    function makeSparse(data) {
        var sparse = [];
        for (day = 0; day < 7; day++) {
            for (hour = 0; hour < 24; hour++) {
                sparse[(day * 24) + hour] = [day, hour, 0]
            }
        }

        for(i = 0; i < data.length; i++) {
            var [day, hour, gameCount] = localize(data[i][0], data[i][1], data[i][2]);
            sparse[((day-1) * 24) + hour][2] = gameCount;
        }

        return sparse;
    }

    sparseData = makeSparse(data);

    // set the dimensions and margins of the graph
    var margin = { top: 40, right: 25, bottom: 30, left: 80 },
        width = 900 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#heatmap")
        .append("svg")
        .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Labels of row and columns
    var dayLabels = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    var hourLabels = [
        "12AM", "1AM", "2AM", "3AM", "4AM", "5AM", "6AM", "7AM", "8AM", "9AM", "10AM", "11AM",
        "12PM", "1PM", "2PM", "3PM", "4PM", "5PM", "6PM", "7PM", "8PM", "9PM", "10PM", "11PM"
    ];

    // Build X scales and axis:
    var x = d3.scaleBand()
        .range([0, width])
        .domain(hourLabels)
        .padding(0);

    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).tickSize(0))
        .select(".domain").remove()

    // Build Y scales and axis:
    var y = d3.scaleBand()
        .range([height, 0])
        .domain(dayLabels)
        .padding(0);

    svg.append("g")
        .call(d3.axisLeft(y).tickSize(0))
        .select(".domain").remove()

    // Build color scale
    var colorScale = d3.scaleSequential()
        .interpolator(d3.interpolateOranges)
        .domain([0, maxGames])

    // Tooltip
    svg.append("text")
        .attr("id", "tooltip")
        .attr("x", 0)
        .attr("y", -10)
        .attr("text-anchor", "left")
        .style("stroke", "#08C")
        .style("fill", "#08C")
        .style("font-size", "22px")
        .style("font-family", "xolonium");

    // Three function that change the tooltip when user hover / move / leave a cell
    var mouseover = function (event, d) {
        d3.select(this)
            .style("stroke", "#444")
            .style("opacity", 0.8)
    }

    var mousemove = function (event, d) {
        d3.select("#tooltip").text(`${dayLabels[d[0]]} @ ${hourLabels[d[1]]}: ${d[2]} games`);
    }

    var mouseleave = function (event, d) {
        d3.select("#tooltip").text("");
        d3.select(this)
            .style("stroke", "none")
            .style("opacity", 0.8)
    }

    // add the squares
    svg.selectAll()
        .data(sparseData, function (d) { return d[2]; })
        .enter()
        .append("rect")
        .attr("x", function (d) { return x(hourLabels[d[1]]) })
        .attr("y", function (d) { return y(dayLabels[d[0]]) })
        .attr("rx", 2)
        .attr("ry", 2)
        .attr("width", x.bandwidth() - 2.5)
        .attr("height", y.bandwidth() - 2.5) 
        .style("fill", function (d) { return colorScale(d[2]) })
        .style("stroke-width", 4)
        .style("stroke", "none")
        .style("opacity", 0.8)
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)
})
module gitlab.com/xonotic/xonstat

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/alehano/reverse v0.0.0-20170809190552-bf1a1f8f8b82
	github.com/antzucaro/qstr v0.0.0-20170524223403-585819fc85f4
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-chi/httprate v0.4.0
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-redis/cache/v8 v8.3.1
	github.com/go-redis/redis/v8 v8.4.4
	github.com/lib/pq v1.6.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/nleeper/goment v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	github.com/swaggo/http-swagger v1.0.0
	github.com/swaggo/swag v1.7.0
	github.com/ungerik/go-cairo v0.0.0-20210317133935-984b32e6bac6
	golang.org/x/net v0.0.0-20210220033124-5f55cee0dc0d // indirect
	golang.org/x/sys v0.0.0-20210220050731-9a76102bfb43 // indirect
	golang.org/x/text v0.3.5
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

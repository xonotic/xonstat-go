package main

import "gitlab.com/xonotic/xonstat/cmd"

func main() {
  cmd.Execute()
}
